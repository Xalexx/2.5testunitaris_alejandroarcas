import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*

internal class _1_9KtTest {

    // 1.9
    @Test
    fun checkWithoutDiscount() {
        assertEquals(0.0, calculaDescompte(1000.0, 1000.0))
    }

    @Test
    fun checkWithDiscountBiggerThan100() {
        assertEquals(-1.0, calculaDescompte(1000.0, -200.0))
    }

    @Test
    fun checkWithNegativeImport() {
        assertEquals(-1.0, calculaDescompte(-1000.0, -800.0))
    }
}