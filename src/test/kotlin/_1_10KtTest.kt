import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*

internal class _1_10KtTest {

    // 1,10
    @Test
    fun checkWithZeroNumber() {
        assertEquals(0.0, calculaMidaPizza(0.0))
    }

    @Test
    fun checkWithNegativeNumber() {
        assertEquals(0.0, calculaMidaPizza(-1.0))
    }

    @Test
    fun checkWithLowNumber() {
        assertEquals(0.7853981633974483, calculaMidaPizza(1.0))
    }
}