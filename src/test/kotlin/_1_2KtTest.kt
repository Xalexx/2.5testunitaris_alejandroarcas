import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*

internal class _1_2KtTest {

    // 1.2
    @Test
    fun checkWithPositiveNumber() {
        assertEquals(8, doblaUnNumero(4))
    }

    @Test
    fun checkWithNegativeNumber(){
        assertEquals(-6, doblaUnNumero(-3))
    }

    @Test
    fun checkWithBigNumber() {
        assertEquals(10000000, doblaUnNumero(5000000))
    }
}