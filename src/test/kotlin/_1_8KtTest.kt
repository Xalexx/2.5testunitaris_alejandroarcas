import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*

internal class _1_8KtTest {

    // 1.8
    @Test
    fun checkWithPositiveNumbers() {
        assertEquals(5.0, doblaElDecimal(2.5))
    }

    @Test
    fun checkWithNegativeNumber() {
        assertEquals(-5.0, doblaElDecimal(-2.5))
    }

    @Test
    fun checkWithZeroNumber() {
        assertEquals(0.0, doblaElDecimal(0.0))
    }
}