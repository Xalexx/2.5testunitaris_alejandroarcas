import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*

internal class _1_3KtTest {

    // 1.3
    @Test
    fun checkWithPositiveAndNegativeNumber() {
        assertEquals(0, sumaDosNombresEnters(-4, 4))
    }

    @Test
    fun checkWithNegativeNumbers() {
        assertEquals(-20, sumaDosNombresEnters(-10, -10))
    }

    @Test
    fun checkWithZero() {
        assertEquals(25, sumaDosNombresEnters(0, 25))
    }
}