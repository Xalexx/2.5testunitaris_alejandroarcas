import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*

internal class _1_16KtTest {

    // 1.16
    @Test
    fun checkWithPositiveNumber() {
        assertEquals(5.0, intToDouble(5))
    }

    @Test
    fun checkWithNegativeNumber(){
        assertEquals(-5.0, intToDouble(-5))
    }

    @Test
    fun checkWithZeroNumber() {
        assertEquals(0.0, intToDouble(0))
    }
}