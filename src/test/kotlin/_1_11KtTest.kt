import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*

internal class _1_11KtTest {

    // 1.11
    @Test
    fun checkWithNegativeNumber() {
        assertEquals(0.0, calculateVolume(7.0, 6.2, -3.2))
    }

    @Test
    fun checkWithZeroNumber() {
        assertEquals(0.0, calculateVolume(0.0, 0.0, 0.0))
    }

    @Test
    fun checkWithPositiveNumber() {
        assertEquals(69.19200000000001, calculateVolume(7.2, 3.1, 3.1))
    }
}