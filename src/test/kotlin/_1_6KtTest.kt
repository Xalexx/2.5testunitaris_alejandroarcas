import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*

internal class _1_6KtTest {

    // 1.6
    @Test
    fun checkWithNegativeStudents() {
        assertEquals(-1, calculatePupitres(-5, 24, 25))
    }

    @Test
    fun checkWith0Student() {
        assertEquals(26, calculatePupitres(0, 25, 25))
    }

    @Test
    fun checkWithJocDeProves() {
        assertEquals(41, calculatePupitres(31, 29, 19))
    }
}