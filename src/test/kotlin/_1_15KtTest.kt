import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*

internal class _1_15KtTest {

    // 1.15
    @Test
    fun checkIfSecondsIsNegative() {
        assertEquals(1, addOneSecond(-5))
    }

    @Test
    fun checkIfSecondsIsGreaterThan60() {
        assertEquals(3, addOneSecond(62))
    }

    @Test
    fun checkWith59Number() {
        assertEquals(0, addOneSecond(59))
    }

}