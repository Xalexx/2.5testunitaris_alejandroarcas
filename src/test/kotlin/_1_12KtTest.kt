import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*

internal class _1_12KtTest {

    // 1.12
    @Test
    fun checkIfIsCorrect() {
        assertEquals(89.78, celsiusToFahrenheit(32.1))
    }

    @Test
    fun checkWithNegativeNumber() {
        assertEquals(26.240000000000002, celsiusToFahrenheit(-3.2))
    }

    @Test
    fun checkWithZeroNumber() {
        assertEquals(32.0, celsiusToFahrenheit(0.0))
    }

}