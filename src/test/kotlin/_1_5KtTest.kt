import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*

internal class _1_5KtTest {

    // 1.5
    @Test
    fun checkWithPositiveNumbers() {
        assertEquals(10, operacioBoja(6, 4, 10, 3))
    }

    @Test
    fun checkWithNegativeNumbers(){
        assertEquals(20, operacioBoja(-7, -3, -6, -4))
    }

    @Test
    fun checkWithPositiveAndNegativeNumbers() {
        assertEquals(-20, operacioBoja(-5, -5, 20, 3))
    }

}