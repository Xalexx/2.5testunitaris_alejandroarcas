import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*

internal class _1_13KtTest {

    // 1.13
    @Test
    fun checkWithPositiveNumber() {
        assertEquals(30.0, sumarTemperaturas(26.7, 3.3))
    }

    @Test
    fun checkWithNegativeIncrease() {
        assertEquals(23.0, sumarTemperaturas(26.0, -3.0))
    }

    @Test
    fun checkWithBigNegativeIncreaseNumber() {
        assertEquals(-3997.5, sumarTemperaturas(3.0, -4000.5))
    }
}