import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*

internal class _1_14KtTest {

    // 1.14
    @Test
    fun checkWithNegativeCostumer() {
        assertEquals(0.0, dividirCuenta(-4, 76.54))
    }

    @Test
    fun checkWithZeroCostumer() {
        assertEquals(0.0, dividirCuenta(0, 654.35))
    }

    @Test
    fun checkWithZeroCost() {
        assertEquals(-1.0, dividirCuenta(4, 0.0))
    }

    @Test
    fun checkWithNegativeCost() {
        assertEquals(0.0, dividirCuenta(4, -16.0))
    }
}