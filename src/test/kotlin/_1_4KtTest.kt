import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*

internal class _1_4KtTest {

    // 1,4
    @Test
    fun checkAreaWithPositiveNumbers() {
        assertEquals(9, calculateArea(3, 3))
    }

    @Test
    fun checkWithNegativeNumbers() {
        assertEquals(0, calculateArea(-5, 4))
    }

    @Test
    fun checkWithBigNumber() {
        assertEquals(428135971041, calculateArea(654321, 654321))
    }
}