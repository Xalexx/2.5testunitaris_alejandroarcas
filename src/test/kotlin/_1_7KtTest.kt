import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*

internal class _1_7KtTest {

    // 1.7
    @Test
    fun checkWithPositiveNumber() {
        assertEquals(6, sumarElNumeroU(5))
    }

    @Test
    fun checkWithZeroNumber() {
        assertEquals(1, sumarElNumeroU(0))
    }

    @Test
    fun checkWithNegativeNumber() {
        assertEquals(-5, sumarElNumeroU(-4))
    }
}