import java.util.Scanner

/*
* AUTHOR: Alejandro Arcas Leon
* DATE: 2022/10/19
* TITLE: Calcula l'àrea
*/
fun calculateArea(width: Long, height: Long) : Long {
    return if (width < 1 || height < 1) 0
    else width*height
}

fun main() {
    val sc = Scanner(System.`in`)
    val width = sc.nextInt()
    val height = sc.nextInt()

    if (calculateArea(width.toLong(), height.toLong()).toInt() == 0) println("Error: Les medidas no pueden ser menor que 1")
    else println("El area de la habitacion es la siguiente: ${calculateArea(width.toLong(), height.toLong())}")
}