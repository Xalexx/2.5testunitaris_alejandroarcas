import java.util.Scanner

/*
* AUTHOR: Alejandro Arcas Leon
* DATE: 2022/10/21
* TITLE: Afegeix un segon
*/

fun addOneSecond(second: Int) : Int {
    return if (second < 0) 1
    else (second+1)%60
}

fun main() {
    val sc = Scanner(System.`in`)

    val seconds = sc.nextInt()
    println(addOneSecond(seconds))
}