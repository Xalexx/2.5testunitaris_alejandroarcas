import java.util.*

/*
* AUTHOR: Alejandro Arcas Leon
* DATE: 2022/10/19
* TITLE: Divisor de compte
*/

fun dividirCuenta(costumer: Int, totalCost: Double) : Double {
    return if (costumer <= 0 || totalCost < 0.0) 0.0
    else if (totalCost == 0.0) -1.0
    else totalCost / costumer
}

fun main() {
    val sc = Scanner(System.`in`)

    val clientes = sc.nextInt()
    val costeTotal = sc.nextDouble()

    if (dividirCuenta(clientes, costeTotal) == 0.0) println("No puede haber 0 clientes o menos, o un coste total menor que 0")
    else if (dividirCuenta(clientes, costeTotal) == -1.0) println("Esta to pagao")
    else println("${dividirCuenta(clientes, costeTotal)}€")
}
