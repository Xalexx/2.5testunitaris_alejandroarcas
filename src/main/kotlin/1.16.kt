import java.util.*

/*
* AUTHOR: Alejandro Arcas Leon
* DATE: 2022/10/21
* TITLE: Transforma l'enter
*/

fun intToDouble(number: Int) : Double {
    return number * 1.0
}

fun main() {
    val sc = Scanner(System.`in`)

    val number = sc.nextInt()

    println(intToDouble(number))
}