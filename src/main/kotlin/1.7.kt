import java.util.*

/*
* AUTHOR: Alejandro Arcas Leon
* DATE: 2022/10/19
* TITLE: Numero seguent
*/

fun sumarElNumeroU(number: Int) : Int {
    return if (number < 0) number-1
    else number+1
}

fun main() {
    val sc = Scanner(System.`in`)
    val number = sc.nextInt()

    println("Despres ve el: ${sumarElNumeroU(number)}")
}