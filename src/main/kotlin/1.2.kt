import java.util.Scanner

/*
* AUTHOR: Alejandro Arcas Leon
* DATE: 2022/10/19
* TITLE: Dobla l'enter
*/
fun doblaUnNumero(number: Int) : Int {
    return number*2
}

fun main() {
    val sc = Scanner(System.`in`)
    println("Introduce un valor entero:")
    val intValue = sc.nextInt()
    println(doblaUnNumero(intValue))
}