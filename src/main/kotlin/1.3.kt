import java.util.Scanner

/*
* AUTHOR: Alejandro Arcas Leon
* DATE: 2022/10/19
* TITLE: Suma de dos nombres enters
*/
fun sumaDosNombresEnters(numA: Int, numB: Int) : Int {
    return numA + numB
}

fun main() {
    val sc = Scanner(System.`in`)

    val firstNumber = sc.nextInt()
    val secondNumber = sc.nextInt()

    println("El resultado de la suma de los numeros es: ${sumaDosNombresEnters(firstNumber, secondNumber)}")
}