import java.util.*

/*
* AUTHOR: Alejandro Arcas Leon
* DATE: 2022/10/19
* TITLE: Quina és la mida de la meva pizza?
*/

fun calculaMidaPizza(diameter: Double) : Double {
    val radius = diameter/2
    return if (diameter <= 0.0) 0.0
    else Math.PI * (radius*radius)
}

fun main() {
    val sc = Scanner(System.`in`)

    val diameter = sc.nextDouble()

    if (calculaMidaPizza(diameter) == 0.0) println("No puede haber una pizza negativa o igual 0 de diametro")
    else println(calculaMidaPizza(diameter))
}