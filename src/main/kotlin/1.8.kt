import java.util.*

/*
* AUTHOR: Alejandro Arcas Leon
* DATE: 2022/10/19
* TITLE: Dobla el decimal
*/

fun doblaElDecimal(number: Double) : Double {
    return number*2.0
}

fun main() {
    val sc = Scanner(System.`in`)

    val number = sc.nextDouble()
    println(doblaElDecimal(number))
}