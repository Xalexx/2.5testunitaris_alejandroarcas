import java.util.*

/*
* AUTHOR: Alejandro Arcas Leon
* DATE: 2022/10/19
* TITLE: Calculadora de Volum d'aire
*/

fun calculateVolume(numA: Double, numB: Double, numC: Double) : Double {
    return if (numA <= 0 || numB <= 0 || numC <= 0) 0.0
    else numA * numB * numC
}

fun main() {
    val sc = Scanner(System.`in`)

    val number1 = sc.nextDouble()
    val number2 = sc.nextDouble()
    val number3 = sc.nextDouble()

    if (calculateVolume(number1, number2, number3) == 0.0) println("Error: no puede haber una medida negativa o igual a 0")
    else println(calculateVolume(number1, number2, number3))
}

