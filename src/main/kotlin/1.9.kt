import java.util.*

/*
* AUTHOR: Alejandro Arcas Leon
* DATE: 2022/10/19
* TITLE: Calcula el descompte
*/

fun calculaDescompte(numA: Double, numB: Double) : Double {
    return if (numB < 0 || numA < 0) return -1.0
    else 100-(100/(numA / numB))
}

fun main() {
    val sc = Scanner(System.`in`)

    val firstNumber = sc.nextDouble()
    val secondNumber = sc.nextDouble()

    if (calculaDescompte(firstNumber, secondNumber) == -1.0) println("No puede haber un valor inicial igual o inferior a 0, o un valor descontado menor que 0")
    else println("${calculaDescompte(firstNumber, secondNumber)}")
}