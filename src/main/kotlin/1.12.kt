import java.util.*

/*
* AUTHOR: Alejandro Arcas Leon
* DATE: 2022/10/19
* TITLE: De Celsius a Fahrenheit
*/

fun celsiusToFahrenheit(number: Double) : Double {
    return (number * 9 / 5) + 32
}

fun main() {
    val sc = Scanner(System.`in`)

    val celsius = sc.nextDouble()
    println("El resultat en graus Fahrenheit es: ${celsiusToFahrenheit(celsius)}")
}