import java.util.*

/*
* AUTHOR: Alejandro Arcas Leon
* DATE: 2022/10/19
* TITLE: Pupitres
*/

fun calculatePupitres(firstClass: Int, secondClass: Int, thirdClass: Int) : Int {
    val opFirstClass = (firstClass / 2) + (firstClass % 2)
    val opSecondClass = (secondClass / 2) + (secondClass % 2)
    val opThirdClass = (thirdClass / 2) + (thirdClass % 2)

    return if (firstClass < 0 || secondClass < 0 || thirdClass < 0) -1
    else opFirstClass+opSecondClass+opThirdClass
}

fun main() {
    val sc = Scanner(System.`in`)

    val firstClass = sc.nextInt()
    val secondClass = sc.nextInt()
    val thirdClass = sc.nextInt()

    if (calculatePupitres(firstClass, secondClass, thirdClass) == -1) println("No puede haber alumnos negativos")
    else println(calculatePupitres(firstClass, secondClass, thirdClass))
}