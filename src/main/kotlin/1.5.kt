import java.util.*

/*
* AUTHOR: Alejandro Arcas Leon
* DATE: 2022/10/19
* TITLE: Operacio boja
*/
fun operacioBoja(numA: Int, numB: Int, numC: Int, numD: Int) : Int {
    return (numA + numB) * (numC % numD)
}

fun main() {
    val sc = Scanner(System.`in`)
    val number1 = sc.nextInt()
    val number2 = sc.nextInt()
    val number3 = sc.nextInt()
    val number4 = sc.nextInt()

    println(operacioBoja(number1, number2, number3, number4))
}